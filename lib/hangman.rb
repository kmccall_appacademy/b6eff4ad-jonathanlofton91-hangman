class Hangman
  attr_reader :guesser, :referee, :board, :dictionary

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end


  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = [nil] * secret_length
  end

  def take_turn
    user_guess = @guesser.guess
    idx_places = @referee.check_guess(user_guess)
    update_board(user_guess, idx_places)

    @guesser.handle_response(user_guess, idx_places)
  end

  def update_board(user_guess, idx_places)
    idx_places.each do |index|
      @board[index] = user_guess
    end
  end


end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      should_delete = false

      word.split("").each_with_index do |letter, index|
        if (letter == guess) && (!response_indices.include?(index))
          should_delete = true
          break
        elsif (letter != guess) && (response_indices.include?(index))
          should_delete = true
          break
        end
      end

      should_delete
    end
  end

  def check_guess(ch)
    word = self.dictionary[0]
    idx_found_ch = []
    word.chars.each_with_index do |letter, idx|
      idx_found_ch << idx if letter == ch
    end
    idx_found_ch
  end

  def guess(board)
    hash = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, index|
        # only count letters at missing positions
        hash[word[index]] += 1 if letter.nil?
      end
    end 



    sorted_hash = hash.sort_by {|key, value| value}
    letter, _ = sorted_hash.last
    letter
  end

  def register_secret_length(length)
    @candidate_words = dictionary.select {|word| word.length == length}
  end
end
